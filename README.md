# Android example : SQLite and more #

**Main branch is dev**

**This program has a lot of inperfections and French. This is because it is a college project. I will translate any file in the repo, all you have to do is ask!**

The app features two main lists : tasks and employees. You can add and edit tasks (name, description, due date, ...) and assigned them to employees. It is also possible to send SMS messages and phone employees directly from the app. Toasts popups warns you about the amount of task due for a certain timespan, which is configurable, along with many other features, in the preferences screen.

This is a single-man college project where I had certain requirements : 

    At least two activities
    A service
    A Broadcast Receiver
    A Content Provider
    A SQLite database, interfaced by the Content Provider
    An option menu
    Preferences
    At least one Toast
    A toolbar
    A fragment
    A ListView working with an Adapter
    Multi-language support
    Dangerous permissions

## How does it work? ##

Well, this is a very simple Android app. I actually put it here mostly as a showoff. Files in src/main/java/net/info420/fabien/androidtravailpratique are pretty well organised and straight-forward. Everything is super JavaDoc'd 'cause I had to much free time. Feel free to ask any question. As previously said, the whole app is **currently all doc'd in French** (see this README's first paragraph).

## Screenshots ##

[Here](https://docs.google.com/presentation/d/1iN1ueqBpr02l5FfCejDwypB_kBnHp5MWS_RHLoPi-6Y/edit?usp=sharing) is a Google Slides presentation I made to show the app. I probably will change sharing settings one day, so if you need to see screenshots and the file is down, send me a message.
